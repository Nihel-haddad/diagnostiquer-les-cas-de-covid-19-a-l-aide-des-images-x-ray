

Covid-19 is a rapidly spreading viral disease that infects not only humans, but animals are also infected because of this disease. The daily life of human beings, their health, and the economy of a country are affected due to this deadly viral disease.
 Covid-19 is a common spreading disease, and till now, not a single country can prepare a vaccine for COVID-19. A clinical study of COVID-19 infected patients has shown that these types of patients are mostly infected from a lung infection after coming in contact with this disease. **Chest x-ray** (i.e., radiography) and chest CT are a more effective imaging technique for diagnosing lunge related problems.

Still, a substantial chest x-ray is a lower cost process in comparison to chest CT. Deep learning is the most successful technique of machine learning, which provides useful analysis to study a large amount of chest x-ray images that can critically impact on screening of Covid-19. In this work, we have taken the PA view of chest x-ray scans for covid-19 affected patients as well as healthy patients. 

After cleaning up the images and applying data augmentation, we have used :
- Deep learning CNN models and compared their performance.

- [ ]  We have compared DenseNet(pre_trained) and custom models and examined their accuracy. 
- Classic machine learning algorithms
- [ ] We use the first layer (CNN) to extract the features from the images. Then we use a classic ML model (xgboost, random forest ...)

To analyze the model performance, 6432 chest x-ray scans samples have been collected from the Kaggle repository, out of which 5467 were used for training and 965 for validation. In result analysis, the xgboost model gives the highest accuracy (i.e., 95.01%) for detecting Chest X-rays images as compared to other models.
